package grpc

import (
	"gitlab.com/final_exam2/warehouse_service/config"
	"gitlab.com/final_exam2/warehouse_service/genproto/warehouse_service"
	"gitlab.com/final_exam2/warehouse_service/grpc/client"
	"gitlab.com/final_exam2/warehouse_service/grpc/service"
	"gitlab.com/final_exam2/warehouse_service/pkg/logger"
	"gitlab.com/final_exam2/warehouse_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	warehouse_service.RegisterArrivalProductServiceServer(grpcServer, service.NewArrivalProductService(cfg, log, strg, srvc))
	warehouse_service.RegisterArrivalServiceServer(grpcServer, service.NewArrivalService(cfg, log, strg, srvc))
	warehouse_service.RegisterRemainderServiceServer(grpcServer, service.NewRemainderService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
