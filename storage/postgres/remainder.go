package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/final_exam2/warehouse_service/genproto/warehouse_service"
	"gitlab.com/final_exam2/warehouse_service/pkg/helper"
)

type RemainderRepo struct {
	db *pgxpool.Pool
}

func NewRemainderRepo(db *pgxpool.Pool) *RemainderRepo {
	return &RemainderRepo{
		db: db,
	}
}

func (r *RemainderRepo) Create(ctx context.Context, req *warehouse_service.RemainderCreate) (*warehouse_service.Remainder, error) {

	var (
		query string
	)

	query = `
		INSERT INTO "remainder"(
			branch_id, 
			category_id, 
			brand_id ,
			product_id,
			 bar_code,
			 count,
			 price,
		  updated_at
		)
		VALUES ($1, $2, $3 , $4 , $5 , $6 , $7, NOW())
	`

	_, err := r.db.Exec(ctx, query,
		req.BranchId,
		req.CategoryId,
		req.BrandId,
		req.ProductId,
		req.BarCode,
		req.Count,
		req.Price,
	)

	if err != nil {
		return nil, err
	}

	return &warehouse_service.Remainder{
		BranchId:   req.BranchId,
		CategoryId: req.CategoryId,
		BrandId:    req.BrandId,
		ProductId:  req.ProductId,
		BarCode:    req.BarCode,
		Count:      req.Count,
		Price:      req.Price,
	}, nil
}

func (r *RemainderRepo) GetByID(ctx context.Context, req *warehouse_service.RemainderPrimaryKey) (*warehouse_service.Remainder, error) {

	var (
		query string

		product_id  sql.NullString
		branch_id   sql.NullString
		category_id sql.NullString
		brand_id    sql.NullString
		bar_code    sql.NullString
		count       int64
		price       float64
		created_at  sql.NullString
		updated_at  sql.NullString
	)

	query = `
		SELECT
		    product_id,
			branch_id,
			category_id,
			brand_id,
			bar_code,
			count,
			price,
			created_at,
			updated_at		
		FROM "remainder"
		WHERE product_id = $1
	`

	err := r.db.QueryRow(ctx, query, req.ProductId).Scan(
		&product_id,
		&branch_id,
		&category_id,
		&brand_id,
		&bar_code,
		&count,
		&price,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &warehouse_service.Remainder{
		ProductId:  product_id.String,
		BranchId:   branch_id.String,
		CategoryId: category_id.String,
		BrandId:    brand_id.String,
		BarCode:    bar_code.String,
		Count:      count,
		Price:      price,
		CreatedAt:  created_at.String,
		UpdatedAt:  updated_at.String,
	}, nil
}

func (r *RemainderRepo) GetList(ctx context.Context, req *warehouse_service.RemainderGetListRequest) (*warehouse_service.RemainderGetListResponse, error) {

	var (
		resp   = &warehouse_service.RemainderGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			branch_id,
			category_id,
			brand_id,
			product_id,
			bar_code,
			count,
			price,
			created_at,
			updated_at		
		FROM remainder
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByBranch != "" {
		where += ` AND branch_id ILIKE '%' || '` + req.SearchByBranch + `' || '%'`
	}

	if req.SearchByBrand != "" {
		where += ` AND brand_id ILIKE '%' || '` + req.SearchByBrand + `' || '%'`
	}

	if req.SearchByCategory != "" {
		where += ` AND category_id ILIKE '%' || '` + req.SearchByCategory + `' || '%'`
	}

	if req.SearchByBarCode != "" {
		where += ` AND bar_code ILIKE '%' || '` + req.SearchByCategory + `' || '%'`
	}

	if req.BranchId != "" {
		where += `AND branch_id ILIKE '%' || '` + req.BranchId + `' || '%'`
	}

	if req.BarCode != "" {
		where += `AND bar_code ILIKE '%' || '` + req.BarCode + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			branch_id   sql.NullString
			category_id sql.NullString
			brand_id    sql.NullString
			product_id  sql.NullString
			bar_code    sql.NullString
			count       sql.NullInt64
			price       sql.NullFloat64
			created_at  sql.NullString
			updated_at  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&branch_id,
			&category_id,
			&brand_id,
			&product_id,
			&bar_code,
			&count,
			&price,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Remainders = append(resp.Remainders, &warehouse_service.Remainder{
			BranchId:   branch_id.String,
			CategoryId: category_id.String,
			BrandId:    brand_id.String,
			ProductId:  product_id.String,
			BarCode:    bar_code.String,
			Count:      count.Int64,
			Price:      price.Float64,
			CreatedAt:  created_at.String,
			UpdatedAt:  updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *RemainderRepo) Update(ctx context.Context, req *warehouse_service.RemainderUpdate) (*warehouse_service.Remainder, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
		"remainder"
		SET
			branch_id = :branch_id,
			category_id = :category_id,
			brand_id = :brand_id,
			product_id = :product_id,
			bar_code = :bar_code,
			count = :count,
			price = :price,
			updated_at = NOW()
		WHERE product_id = :product_id
	`

	params = map[string]interface{}{
		"branch_id":   req.GetBranchId(),
		"category_id": req.GetCategoryId(),
		"brand_id":    req.GetBrandId(),
		"product_id":  req.GetProductId(),
		"bar_code":    req.GetBarCode(),
		"count":       req.GetCount(),
		"price":       req.GetPrice(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &warehouse_service.Remainder{
		BranchId:   req.BranchId,
		CategoryId: req.CategoryId,
		BrandId:    req.BrandId,
		ProductId:  req.ProductId,
		BarCode:    req.BarCode,
		Count:      req.Count,
		Price:      req.Price,
	}, nil
}

func (r *RemainderRepo) Delete(ctx context.Context, req *warehouse_service.RemainderPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM remainder WHERE product_id = $1", req.ProductId)
	if err != nil {
		return err
	}

	return nil
}
