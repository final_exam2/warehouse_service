
CREATE TABLE "arrival_product" (
 "id" UUID PRIMARY KEY,
  "category_id" UUID  NOT NULL,
  "brand_id" UUID  NOT NULL,
  "product_id" UUID  NOT NULL,
  "bar_code" varchar unique,
  "count" int,
  "price" NUMERIC ,
  "arrival_id" UUID  NOT NULL,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);


CREATE TABLE "arrival" (
  "id" UUID PRIMARY KEY ,
  "branch_id" UUID NOT NULL,
  "provider_id"  UUID  NOT NULL,
  "status" varchar DEFAULT 'in_proccess',
  "arrival_id" varchar unique,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);



CREATE TABLE "remainder" (
  "branch_id" UUID  NOT NULL,
  "category_id" UUID  NOT NULL,
  "brand_id" UUID  NOT NULL,
  "product_id" UUID  PRIMARY KEY,
  "bar_code" varchar unique,
  "count" int,
  "price" NUMERIC ,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);