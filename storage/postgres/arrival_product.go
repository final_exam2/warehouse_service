package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/final_exam2/warehouse_service/genproto/warehouse_service"
	"gitlab.com/final_exam2/warehouse_service/pkg/helper"
)

type ArrivalProductRepo struct {
	db *pgxpool.Pool
}

func NewArrivalProductRepo(db *pgxpool.Pool) *ArrivalProductRepo {
	return &ArrivalProductRepo{
		db: db,
	}
}

func (r *ArrivalProductRepo) Create(ctx context.Context, req *warehouse_service.ArrivalProductCreate) (*warehouse_service.ArrivalProduct, error) {

	var (
		id    = uuid.New().String()
		query string
	)

	query = `
		INSERT INTO arrival_product(id, category_id, brand_id , product_id, bar_code, count , price, arrival_id, updated_at)
		VALUES ($1, $2, $3,$4,$5, $6, $7, $8,NOW())
	`

	_, err := r.db.Exec(ctx, query,
		id,
		helper.NewNullString(req.CategoryId),
		helper.NewNullString(req.BrandId),
		helper.NewNullString(req.ProductId),
		req.BarCode,
		req.Count,
		req.Price,
		helper.NewNullString(req.ArrivalId),
	)

	if err != nil {
		return nil, err
	}

	return &warehouse_service.ArrivalProduct{
		Id:         id,
		CategoryId: req.CategoryId,
		BrandId:    req.BrandId,
		ProductId:  req.ProductId,
		BarCode:    req.BarCode,
		Count:      req.Count,
		Price:      req.Price,
		ArrivalId:  req.ArrivalId,
	}, nil
}

func (r *ArrivalProductRepo) GetByID(ctx context.Context, req *warehouse_service.ArrivalProductPrimaryKey) (*warehouse_service.ArrivalProduct, error) {

	var (
		query string

		id          sql.NullString
		category_id sql.NullString
		brand_id    sql.NullString
		product_id  sql.NullString
		bar_code    sql.NullString
		count       int64
		price       float64
		arrival_id  sql.NullString
		created_at  sql.NullString
		updated_at  sql.NullString
	)

	query = `
		SELECT
			id,
			category_id,
			brand_id,
			product_id,
			bar_code,
			count,
			price,
			arrival_id,
			created_at,
			updated_at		
		FROM arrival_product
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&category_id,
		&brand_id,
		&product_id,
		&bar_code,
		&count,
		&price,
		&arrival_id,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &warehouse_service.ArrivalProduct{
		Id:         id.String,
		CategoryId: category_id.String,
		BrandId:    brand_id.String,
		ProductId:  product_id.String,
		BarCode:    bar_code.String,
		Count:      count,
		Price:      price,
		ArrivalId:  arrival_id.String,
		CreatedAt:  created_at.String,
		UpdatedAt:  updated_at.String,
	}, nil
}

func (r *ArrivalProductRepo) GetList(ctx context.Context, req *warehouse_service.ArrivalProductGetListRequest) (*warehouse_service.ArrivalProductGetListResponse, error) {

	var (
		resp   = &warehouse_service.ArrivalProductGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			category_id,
			brand_id,
			product_id,
			bar_code,
			count,
			price,
			arrival_id,
			created_at,
			updated_at		
		FROM arrival_product
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByBranch != "" {
		where += ` AND branch_id ILIKE '%' || '` + req.SearchByBranch + `' || '%'`
	}

	if req.SearchByBrand != "" {
		where += ` AND brand_id ILIKE '%' || '` + req.SearchByBrand + `' || '%'`
	}

	if req.SearchByCategory != "" {
		where += ` AND category_id ILIKE '%' || '` + req.SearchByCategory + `' || '%'`
	}

	if req.SearchByBarCode != "" {
		where += ` AND bar_code ILIKE '%' || '` + req.SearchByCategory + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			category_id sql.NullString
			brand_id    sql.NullString
			product_id  sql.NullString
			bar_code    sql.NullString
			count       sql.NullInt64
			price       sql.NullFloat64
			arrival_id  sql.NullString
			created_at  sql.NullString
			updated_at  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&category_id,
			&brand_id,
			&product_id,
			&bar_code,
			&count,
			&price,
			&arrival_id,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.ArrivalProducts = append(resp.ArrivalProducts, &warehouse_service.ArrivalProduct{
			Id:         id.String,
			CategoryId: category_id.String,
			BrandId:    brand_id.String,
			ProductId:  product_id.String,
			BarCode:    bar_code.String,
			Count:      count.Int64,
			Price:      price.Float64,
			ArrivalId:  arrival_id.String,
			CreatedAt:  created_at.String,
			UpdatedAt:  updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *ArrivalProductRepo) Update(ctx context.Context, req *warehouse_service.ArrivalProductUpdate) (*warehouse_service.ArrivalProduct, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
		arrival_product
		SET
			category_id = :category_id,
			brand_id = :brand_id,
			product_id =: product_id,
			bar_code = :bar_code,
			count =: count,
			price = :price,
			arrival_id =: arrival_id,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":          req.GetId(),
		"category_id": req.GetCategoryId(),
		"brand_id":    req.GetBrandId(),
		"product_id":  req.GetProductId(),
		"bar_code":    req.GetBarCode(),
		"count":       req.GetCount(),
		"price":       req.GetPrice(),
		"arrival_id":  req.GetArrivalId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &warehouse_service.ArrivalProduct{
		Id:         req.Id,
		CategoryId: req.CategoryId,
		BrandId:    req.BrandId,
		ProductId:  req.ProductId,
		BarCode:    req.BarCode,
		Count:      req.Count,
		Price:      req.Price,
	}, nil
}

func (r *ArrivalProductRepo) Delete(ctx context.Context, req *warehouse_service.ArrivalProductPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM arrival_product WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
