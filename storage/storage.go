package storage

import (
	"context"

	"gitlab.com/final_exam2/warehouse_service/genproto/warehouse_service"
)

type StorageI interface {
	CloseDB()
	ArrivalProduct() ArrivalProductRepoI
	Arrival() ArrivalRepoI
	Remainder() RemainderRepoI
}

type ArrivalProductRepoI interface {
	Create(context.Context, *warehouse_service.ArrivalProductCreate) (*warehouse_service.ArrivalProduct, error)
	GetByID(context.Context, *warehouse_service.ArrivalProductPrimaryKey) (*warehouse_service.ArrivalProduct, error)
	GetList(context.Context, *warehouse_service.ArrivalProductGetListRequest) (*warehouse_service.ArrivalProductGetListResponse, error)
	Update(context.Context, *warehouse_service.ArrivalProductUpdate) (*warehouse_service.ArrivalProduct, error)
	Delete(context.Context, *warehouse_service.ArrivalProductPrimaryKey) error
}

type ArrivalRepoI interface {
	Create(context.Context, *warehouse_service.ArrivalCreate) (*warehouse_service.Arrival, error)
	GetByID(context.Context, *warehouse_service.ArrivalPrimaryKey) (*warehouse_service.Arrival, error)
	GetList(context.Context, *warehouse_service.ArrivalGetListRequest) (*warehouse_service.ArrivalGetListResponse, error)
	Update(context.Context, *warehouse_service.ArrivalUpdate) (*warehouse_service.Arrival, error)
	Delete(context.Context, *warehouse_service.ArrivalPrimaryKey) error
}

type RemainderRepoI interface {
	Create(context.Context, *warehouse_service.RemainderCreate) (*warehouse_service.Remainder, error)
	GetByID(context.Context, *warehouse_service.RemainderPrimaryKey) (*warehouse_service.Remainder, error)
	GetList(context.Context, *warehouse_service.RemainderGetListRequest) (*warehouse_service.RemainderGetListResponse, error)
	Update(context.Context, *warehouse_service.RemainderUpdate) (*warehouse_service.Remainder, error)
	Delete(context.Context, *warehouse_service.RemainderPrimaryKey) error
}
