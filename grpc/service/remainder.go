package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/final_exam2/warehouse_service/config"
	"gitlab.com/final_exam2/warehouse_service/genproto/warehouse_service"
	"gitlab.com/final_exam2/warehouse_service/grpc/client"
	"gitlab.com/final_exam2/warehouse_service/pkg/logger"
	"gitlab.com/final_exam2/warehouse_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type RemainderService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*warehouse_service.UnimplementedRemainderServiceServer
}

func NewRemainderService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *RemainderService {
	return &RemainderService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *RemainderService) Create(ctx context.Context, req *warehouse_service.RemainderCreate) (*warehouse_service.Remainder, error) {
	u.log.Info("====== Remainder Create ======", logger.Any("req", req))

	resp, err := u.strg.Remainder().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Remainder: u.strg.Remainder().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *RemainderService) GetById(ctx context.Context, req *warehouse_service.RemainderPrimaryKey) (*warehouse_service.Remainder, error) {
	u.log.Info("====== Remainder Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Remainder().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Remainder Get By ID: u.strg.Remainder().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *RemainderService) GetList(ctx context.Context, req *warehouse_service.RemainderGetListRequest) (*warehouse_service.RemainderGetListResponse, error) {
	u.log.Info("====== Remainder Get List ======", logger.Any("req", req))

	resp, err := u.strg.Remainder().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Remainder Get List: u.strg.Remainder().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *RemainderService) Update(ctx context.Context, req *warehouse_service.RemainderUpdate) (*warehouse_service.Remainder, error) {
	u.log.Info("====== Product Update ======", logger.Any("req", req))

	resp, err := u.strg.Remainder().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Remainder Update: u.strg.Remainder().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *RemainderService) Delete(ctx context.Context, req *warehouse_service.RemainderPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Remainder Delete ======", logger.Any("req", req))

	err := u.strg.Remainder().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Remainder Delete: u.strg.Remainder().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
