package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/final_exam2/warehouse_service/config"
	"gitlab.com/final_exam2/warehouse_service/genproto/warehouse_service"
	"gitlab.com/final_exam2/warehouse_service/grpc/client"
	"gitlab.com/final_exam2/warehouse_service/pkg/logger"
	"gitlab.com/final_exam2/warehouse_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type ArrivalProductService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*warehouse_service.UnimplementedArrivalProductServiceServer
}

func NewArrivalProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ArrivalProductService {
	return &ArrivalProductService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *ArrivalProductService) Create(ctx context.Context, req *warehouse_service.ArrivalProductCreate) (*warehouse_service.ArrivalProduct, error) {
	u.log.Info("====== ArrivalProduct Create ======", logger.Any("req", req))

	resp, err := u.strg.ArrivalProduct().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create ArrivalProduct: u.strg.ArrivalProduct().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ArrivalProductService) GetById(ctx context.Context, req *warehouse_service.ArrivalProductPrimaryKey) (*warehouse_service.ArrivalProduct, error) {
	u.log.Info("====== ArrivalProduct Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.ArrivalProduct().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While ArrivalProduct Get By ID: u.strg.ArrivalProduct().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ArrivalProductService) GetList(ctx context.Context, req *warehouse_service.ArrivalProductGetListRequest) (*warehouse_service.ArrivalProductGetListResponse, error) {
	u.log.Info("====== ArrivalProduct Get List ======", logger.Any("req", req))

	resp, err := u.strg.ArrivalProduct().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While ArrivalProduct Get List: u.strg.ArrivalProduct().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ArrivalProductService) Update(ctx context.Context, req *warehouse_service.ArrivalProductUpdate) (*warehouse_service.ArrivalProduct, error) {
	u.log.Info("====== Product Update ======", logger.Any("req", req))

	resp, err := u.strg.ArrivalProduct().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While ArrivalProduct Update: u.strg.ArrivalProduct().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ArrivalProductService) Delete(ctx context.Context, req *warehouse_service.ArrivalProductPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== ArrivalProduct Delete ======", logger.Any("req", req))

	err := u.strg.ArrivalProduct().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While ArrivalProduct Delete: u.strg.ArrivalProduct().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
