package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/final_exam2/warehouse_service/config"
	"gitlab.com/final_exam2/warehouse_service/storage"
)

type Store struct {
	db             *pgxpool.Pool
	arrivalproduct *ArrivalProductRepo
	arrival        *ArrivalRepo
	remainder      *RemainderRepo
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",
			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, nil

}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) ArrivalProduct() storage.ArrivalProductRepoI {
	if s.arrivalproduct == nil {
		s.arrivalproduct = NewArrivalProductRepo(s.db)
	}

	return s.arrivalproduct
}

func (s *Store) Arrival() storage.ArrivalRepoI {
	if s.arrival == nil {
		s.arrival = NewArrivalRepo(s.db)
	}

	return s.arrival
}

func (s *Store) Remainder() storage.RemainderRepoI {
	if s.remainder == nil {
		s.remainder = NewRemainderRepo(s.db)
	}

	return s.remainder
}
