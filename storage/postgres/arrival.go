package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/final_exam2/warehouse_service/genproto/warehouse_service"
	"gitlab.com/final_exam2/warehouse_service/pkg/helper"
)

type ArrivalRepo struct {
	db *pgxpool.Pool
}

func NewArrivalRepo(db *pgxpool.Pool) *ArrivalRepo {
	return &ArrivalRepo{
		db: db,
	}
}

func (r *ArrivalRepo) Create(ctx context.Context, req *warehouse_service.ArrivalCreate) (*warehouse_service.Arrival, error) {
	count, err := r.GetList(ctx, &warehouse_service.ArrivalGetListRequest{})
	if err != nil {
		return nil, err
	}
	var (
		id         = uuid.New().String()
		query      string
		arrival_id = helper.GenerateString("A", int(count.Count)+1)
	)

	query = `
		INSERT INTO arrival(id, branch_id, provider_id,arrival_id ,updated_at)
		VALUES ($1, $2, $3,$4 ,NOW())
	`

	_, err = r.db.Exec(ctx, query,
		id,
		req.BranchId,
		req.ProviderId,
		arrival_id,
	)

	if err != nil {
		return nil, err
	}

	return &warehouse_service.Arrival{
		Id:         id,
		BranchId:   req.BranchId,
		ProviderId: req.ProviderId,
		ArrivalId:  arrival_id,
	}, nil
}

func (r *ArrivalRepo) GetByID(ctx context.Context, req *warehouse_service.ArrivalPrimaryKey) (*warehouse_service.Arrival, error) {

	var (
		query string

		id          sql.NullString
		branch_id   sql.NullString
		provider_id sql.NullString
		status      sql.NullString
		arrival_id  sql.NullString
		created_at  sql.NullString
		updated_at  sql.NullString
	)

	query = `
		SELECT
			id,
			branch_id,
			provider_id,
			status,
			arrival_id,
			created_at,
			updated_at		
		FROM arrival
		WHERE id = $1
	`

	err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branch_id,
		&provider_id,
		&status,
		&arrival_id,
		&created_at,
		&updated_at,
	)

	if err != nil {
		return nil, err
	}

	return &warehouse_service.Arrival{
		Id:         id.String,
		BranchId:   branch_id.String,
		ProviderId: provider_id.String,
		Status:     status.String,
		ArrivalId:  arrival_id.String,
		CreatedAt:  created_at.String,
		UpdatedAt:  updated_at.String,
	}, nil
}

func (r *ArrivalRepo) GetList(ctx context.Context, req *warehouse_service.ArrivalGetListRequest) (*warehouse_service.ArrivalGetListResponse, error) {

	var (
		resp   = &warehouse_service.ArrivalGetListResponse{}
		query  string
		where  = " WHERE TRUE"
		offset = " OFFSET 0"
		limit  = " LIMIT 10"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			provider_id,
			status,
			arrival_id,
			created_at,
			updated_at		
		FROM arrival
	`

	if req.Offset > 0 {
		offset = fmt.Sprintf(" OFFSET %d", req.Offset)
	}

	if req.Limit > 0 {
		limit = fmt.Sprintf(" LIMIT %d", req.Limit)
	}

	if req.SearchByBranch != "" {
		where += ` AND branch_id ILIKE '%' || '` + req.SearchByBranch + `' || '%'`
	}

	query += where + offset + limit

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			branch_id   sql.NullString
			provider_id sql.NullString
			status      sql.NullString
			arrival_id  sql.NullString
			created_at  sql.NullString
			updated_at  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branch_id,
			&provider_id,
			&status,
			&arrival_id,
			&created_at,
			&updated_at,
		)

		if err != nil {
			return nil, err
		}

		resp.Arrivals = append(resp.Arrivals, &warehouse_service.Arrival{
			Id:         id.String,
			BranchId:   branch_id.String,
			ProviderId: provider_id.String,
			Status:     status.String,
			ArrivalId:  arrival_id.String,
			CreatedAt:  created_at.String,
			UpdatedAt:  updated_at.String,
		})
	}
	rows.Close()

	return resp, nil
}

func (r *ArrivalRepo) Update(ctx context.Context, req *warehouse_service.ArrivalUpdate) (*warehouse_service.Arrival, error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
		arrival
		SET
			branch_id = :branch_id,
			provider_id = :provider_id,
			status =: status,
			arrival_id =:arrival_id,
			updated_at = NOW()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":          req.GetId(),
		"branch_id":   req.GetBranchId(),
		"provider_id": req.GetProviderId(),
		"status":      req.GetStatus(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return nil, err
	}

	if result.RowsAffected() == 0 {
		return nil, nil
	}

	return &warehouse_service.Arrival{
		Id:         req.Id,
		BranchId:   req.BranchId,
		ProviderId: req.ProviderId,
		Status:     req.Status,
	}, nil
}

func (r *ArrivalRepo) Delete(ctx context.Context, req *warehouse_service.ArrivalPrimaryKey) error {

	_, err := r.db.Exec(ctx, "DELETE FROM arrival WHERE id = $1", req.Id)
	if err != nil {
		return err
	}

	return nil
}
