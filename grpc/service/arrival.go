package service

import (
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.com/final_exam2/warehouse_service/config"
	"gitlab.com/final_exam2/warehouse_service/genproto/warehouse_service"
	"gitlab.com/final_exam2/warehouse_service/grpc/client"
	"gitlab.com/final_exam2/warehouse_service/pkg/logger"
	"gitlab.com/final_exam2/warehouse_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type ArrivalService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*warehouse_service.UnimplementedArrivalServiceServer
}

func NewArrivalService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ArrivalService {
	return &ArrivalService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *ArrivalService) Create(ctx context.Context, req *warehouse_service.ArrivalCreate) (*warehouse_service.Arrival, error) {
	u.log.Info("====== Arrival Create ======", logger.Any("req", req))

	resp, err := u.strg.Arrival().Create(ctx, req)
	if err != nil {
		u.log.Error("Error While Create Arrival: u.strg.Arrival().Create", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ArrivalService) GetById(ctx context.Context, req *warehouse_service.ArrivalPrimaryKey) (*warehouse_service.Arrival, error) {
	u.log.Info("====== Arrival Get By Id ======", logger.Any("req", req))

	resp, err := u.strg.Arrival().GetByID(ctx, req)
	if err != nil {
		u.log.Error("Error While Arrival Get By ID: u.strg.Arrival().GetByID", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ArrivalService) GetList(ctx context.Context, req *warehouse_service.ArrivalGetListRequest) (*warehouse_service.ArrivalGetListResponse, error) {
	u.log.Info("====== Arrival Get List ======", logger.Any("req", req))

	resp, err := u.strg.Arrival().GetList(ctx, req)
	if err != nil {
		u.log.Error("Error While Arrival Get List: u.strg.Arrival().GetList", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ArrivalService) Update(ctx context.Context, req *warehouse_service.ArrivalUpdate) (*warehouse_service.Arrival, error) {
	u.log.Info("====== Product Update ======", logger.Any("req", req))

	resp, err := u.strg.Arrival().Update(ctx, req)
	if err != nil {
		u.log.Error("Error While Arrival Update: u.strg.Arrival().Update", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *ArrivalService) Delete(ctx context.Context, req *warehouse_service.ArrivalPrimaryKey) (*emptypb.Empty, error) {
	u.log.Info("====== Arrival Delete ======", logger.Any("req", req))

	err := u.strg.Arrival().Delete(ctx, req)
	if err != nil {
		u.log.Error("Error While Arrival Delete: u.strg.Arrival().Delete", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
